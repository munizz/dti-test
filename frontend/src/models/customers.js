import Axios from "axios";
import { notification } from "antd";
const BASE_URL = "http://localhost:8080/api";

export interface Customer {
	id: string;
	name: string;
	address: string;
	phone: string;
	email: string;
	cpf: string;
}

export const getAllCustomers = () =>
	Axios.get(`${BASE_URL}/customers`).then((success) => success.data);
export const getCustomerById = (id) => Axios.get(`${BASE_URL}/customers/${id}`);
export const getCustomerByCpf = (cpf) =>
	Axios.get(`${BASE_URL}/customers/cpf/${cpf}`);
export const deleteCustomerById = (id) =>
	Axios.delete(`${BASE_URL}/customers/${id}`)
		.then((success) => {
			notification.success({
				message: "Customer has been deleted successfully!",
				duration: 2,
			});

			return success.data;
		})
		.catch((error) =>
			notification.error({
				message: "Ops! an error occurred =(",
				description: error,
			})
		);
export const deleteCustomerByCpf = (cpf) =>
	Axios.delete(`${BASE_URL}/customers/cpf/${cpf}`);
export const updateCustomer = (customer, id) =>
	Axios.put(`${BASE_URL}/customers/${id}`, customer).then((success) => {
		notification.success({
			message: "Customer has been updated successfully!",
			duration: 2,
		});

		return success.data;
	});
export const createCustomer = (customer) =>
	Axios.post(`${BASE_URL}/customers`, customer).then((success) => {
		notification.success({
			message: "Customer has been created successfully!",
			duration: 2,
		});

		return success.data;
	});
