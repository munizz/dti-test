import React, { useContext, useState, useEffect, useRef } from "react";
import ReactDOM from "react-dom";
import "antd/dist/antd.css";
import "./index.css";
import { Layout, Menu, Table, Input, Button, Popconfirm, Form } from "antd";
import {
	createCustomer,
	getAllCustomers,
	updateCustomer,
	deleteCustomerById,
} from "./models/customers";
import { buildCpf, buildPhoneMask } from "./helpers/index";

const { Header, Content } = Layout;
const EditableContext = React.createContext();

const EditableRow = ({ index, ...props }) => {
	const [form] = Form.useForm();
	return (
		<Form form={form} component={false}>
			<EditableContext.Provider value={form}>
				<tr {...props} />
			</EditableContext.Provider>
		</Form>
	);
};

const EditableCell = ({
	title,
	editable,
	children,
	dataIndex,
	record,
	handleSave,
	...restProps
}) => {
	const [editing, setEditing] = useState(false);
	const inputRef = useRef();
	const form = useContext(EditableContext);
	useEffect(() => {
		if (editing) {
			inputRef.current.focus();
		}
	}, [editing]);

	const toggleEdit = () => {
		setEditing(!editing);
		form.setFieldsValue({
			[dataIndex]: record[dataIndex],
		});
	};

	const save = async (e) => {
		try {
			const values = await form.validateFields();
			toggleEdit();
			handleSave({ ...record, ...values });
		} catch (errInfo) {
			console.log("Save failed:", errInfo);
		}
	};

	let childNode = children;

	const getInputLength = (title) => {
		const lengthTypes = [
			{
				title: "*CPF",
				length: 11,
			},
			{
				title: "Phone",
				length: 13,
			},
		];

		let type = lengthTypes.find((l) => l.title === title);

		if (type === undefined) return 100;
		return type.length;
	};

	if (editable) {
		childNode = editing ? (
			<Form.Item
				style={{
					margin: 0,
				}}
				name={dataIndex}
				rules={[
					{
						required: true,
						message: `${title} is required.`,
					},
				]}
			>
				{getInputLength(title) !== undefined ? (
					<Input
						ref={inputRef}
						onPressEnter={save}
						onBlur={save}
						maxLength={getInputLength(title)}
					/>
				) : (
					<Input ref={inputRef} onPressEnter={save} onBlur={save} />
				)}
			</Form.Item>
		) : (
			<div
				className="editable-cell-value-wrap"
				style={{
					paddingRight: 24,
				}}
				onClick={toggleEdit}
			>
				{children}
			</div>
		);
	}

	return <td {...restProps}>{childNode}</td>;
};

class EditableTable extends React.Component {
	constructor(props) {
		super(props);

		this.columns = [
			{
				title: "*Name",
				dataIndex: "name",
				key: "name",
				width: "15%",
				editable: true,
				align: "left",
			},
			{
				title: "Address",
				dataIndex: "address",
				key: "address",
				width: "15%",
				editable: true,
			},
			{
				title: "Phone",
				dataIndex: "phone",
				key: "phone",
				width: "20%",
				editable: true,
				render: (phone, record) => <span>{buildPhoneMask(phone)}</span>,
			},
			{
				title: "E-mail",
				dataIndex: "email",
				key: "email",
				width: "15%",
				editable: true,
			},
			{
				title: "*CPF",
				dataIndex: "cpf",
				key: "cpf",
				width: "40%",
				editable: true,
				render: (cpf, record) => <span>{buildCpf(cpf)}</span>,
			},
			{
				title: "Action",
				dataIndex: "operation",
				render: (text, record) =>
					this.state.dataSource.length >= 1 ? (
						<Popconfirm
							title="Sure to delete?"
							onConfirm={() => handleDelete(record.id)}
						>
							<button type="button">Delete</button>
						</Popconfirm>
					) : null,
			},
		];

		this.state = {
			dataSource: [],
			count: 0,
		};

		const handleDelete = (id) => {
			if (id !== "") {
				const dataSource = [...this.state.dataSource];
				const result = deleteCustomerById(id);
				this.setState({
					dataSource: dataSource.filter((item) => item.id !== id),
				});
			}
		};
	}

	componentDidMount() {
		getAllCustomers().then((customers) => {
			this.setState({ dataSource: customers });
		});
	}

	fetchAllCustomers = () => {
		getAllCustomers().then((customers) => {
			this.setState({ dataSource: customers });
		});
	};

	render() {
		const { dataSource } = this.state;

		const components = {
			body: {
				row: EditableRow,
				cell: EditableCell,
			},
		};

		const handleAdd = () => {
			console.log("ADD ROW CALLED");
			const { count, dataSource } = this.state;
			const newData = {
				id: "",
				name: "empty",
				address: "empty",
				phone: "553199999999",
				email: "empty",
				cpf: "00000000000",
			};
			this.setState({
				dataSource: [...dataSource, newData],
				count: count + 1,
			});
		};

		const handleSave = (row) => {
			if (row["id"] !== "") {
				const newData = [...this.state.dataSource];
				const index = newData.findIndex((item) => row.id === item.id);
				const item = newData[index];
				newData.splice(index, 1, { ...item, ...row });

				const result = updateCustomer(row, row["id"]);

				this.setState({ dataSource: newData });
			} else if (row["name"] !== "empty" && row["cpf"] !== "00000000000") {
				const result = createCustomer(row);
			}

			const newData = [...this.state.dataSource];
			const index = newData.findIndex((item) => row.id === item.id);
			const item = newData[index];
			newData.splice(index, 1, { ...item, ...row });
			this.setState({ dataSource: newData });
		};

		const columns = this.columns.map((col) => {
			if (!col.editable) {
				return col;
			}

			return {
				...col,
				onCell: (record) => ({
					record,
					editable: col.editable,
					dataIndex: col.dataIndex,
					title: col.title,
					handleSave: handleSave,
				}),
			};
		});

		return (
			<>
				<Layout>
					<Header className="layout-header">
						<Menu
							theme="dark"
							mode="horizontal"
							defaultSelectedKeys={[""]}
						></Menu>
					</Header>
					<Content
						className="site-layout"
						style={{ padding: "0 50px", marginTop: 64 }}
					>
						<div
							className="site-layout-background"
							style={{ padding: 24, minHeight: 380 }}
						>
							<div>
								<Button
									onClick={handleAdd}
									type="primary"
									style={{
										marginBottom: 16,
									}}
								>
									New Customer
								</Button>
								<Table
									components={components}
									rowClassName={() => "editable-row"}
									bordered
									dataSource={dataSource}
									columns={columns}
									rowKey="id"
									pagination={{ pageSize: 5 }}
								/>
							</div>
						</div>
					</Content>
				</Layout>
			</>
		);
	}
}

ReactDOM.render(<EditableTable />, document.getElementById("root"));
