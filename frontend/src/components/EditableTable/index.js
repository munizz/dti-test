import React, { useContext, useState, useEffect, useRef } from "react";
import ReactDOM from "react-dom";
import "antd/dist/antd.css";
import "./index.css";
import { Table, Input, Button, Popconfirm, Form } from "antd";
import { Layout, Menu, Breadcrumb } from "antd";
import { getAllCustomers } from "./models/customers";
import EditableCell from "../EditableCell";
import EditableRow from "../EditableRow";

const EditableTable = () => {
	const [dataSource, setDataSource] = useState([]);
	const [count, setCount] = useState(0);

	const columns = [
		{
			title: "name",
			dataIndex: "name",
			key: "name",
			width: "30%",
			editable: true,
		},
		{
			title: "address",
			dataIndex: "address",
			key: "address",
			width: "30%",
			editable: true,
		},
		{
			title: "phone",
			dataIndex: "phone",
			key: "phone",
			width: "30%",
			editable: true,
		},
		{
			title: "email",
			dataIndex: "email",
			key: "email",
			width: "30%",
			editable: true,
		},
		{
			title: "cpf",
			dataIndex: "cpf",
			key: "cpf",
			width: "30%",
			editable: true,
		},
		{
			title: "operation",
			dataIndex: "operation",
			render: (text, record) =>
				this.state.dataSource.length >= 1 ? (
					<Popconfirm
						title="Sure to delete?"
						onConfirm={() => console.log(record)}
						// onConfirm={() => this.handleDelete(record.id)}
					>
						<a>Delete</a>
					</Popconfirm>
				) : null,
		},
	];

	useEffect(() => {
		const getData = async () => {
			const result = await getAllCustomers();
			if (result) setDataSource(result.data);
		};

		getData();
	}, []);

	const handleDelete = (id) => {
		const dataSource = [...this.state.dataSource];
		this.setState({
			dataSource: dataSource.filter((item) => item.id !== id),
		});
	};

	const handleAdd = () => {
		const newData = {
			name: "",
			address: "",
			phone: "",
			email: "",
			cpf: "",
		};
		setDataSource([...dataSource, newData]);
		setCount(count + 1);
	};

	const handleSave = (row) => {
		const newData = [...this.state.dataSource];
		const index = newData.findIndex((item) => row.id === item.id);
		const item = newData[index];
		newData.splice(index, 1, { ...item, ...row });

		setDataSource(newData);
	};

	const components = {
		body: {
			row: EditableRow,
			cell: EditableCell,
		},
	};

	const columnsWithProps = columns.map((col) => {
		if (!col.editable) {
			return col;
		}

		return {
			...col,
			onCell: (record) => ({
				record,
				editable: col.editable,
				dataIndex: col.dataIndex,
				title: col.title,
				handleSave: handleSave,
			}),
		};
	});

	return (
		<>
			<Layout>
				<Header className="layout-header">
					<Menu theme="dark" mode="horizontal" defaultSelectedKeys={[""]}>
						<Menu.Item key="1">Customer Manager</Menu.Item>
					</Menu>
				</Header>
				<Content
					className="site-layout"
					style={{ padding: "0 50px", marginTop: 64 }}
				>
					<Breadcrumb style={{ margin: "16px 0" }}>
						<Breadcrumb.Item>Home</Breadcrumb.Item>
						<Breadcrumb.Item>List</Breadcrumb.Item>
						<Breadcrumb.Item>App</Breadcrumb.Item>
					</Breadcrumb>
					<div
						className="site-layout-background"
						style={{ padding: 24, minHeight: 380 }}
					>
						<div>
							<Button
								onClick={handleAdd()}
								type="primary"
								style={{
									marginBottom: 16,
								}}
							>
								Add a row
							</Button>
							<Table
								components={components}
								rowClassName={() => "editable-row"}
								bordered
								dataSource={dataSource}
								columns={columnsWithProps}
							/>
						</div>
					</div>
				</Content>
			</Layout>
		</>
	);
};

export default EditableTable;
