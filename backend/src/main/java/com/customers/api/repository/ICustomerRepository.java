package com.customers.api.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.customers.api.models.Customer;

@Repository
public interface ICustomerRepository extends JpaRepository<Customer, UUID>{

	public Optional<Customer> findById(UUID id);
	
	@Query("FROM Customer WHERE cpf = ?1")
	public Optional<Customer> findByCpf(String cpf);	
}
