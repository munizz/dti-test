package com.customers.api.resources;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.customers.api.models.Customer;
import com.customers.api.repository.ICustomerRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value="/api")
@Api(value = "Customer API")
@CrossOrigin(origins="*")
public class CustomerResource {

	@Autowired
	ICustomerRepository _customerRepository;
	
	@GetMapping("/customers")
	@ApiOperation(value = "Returns all customers in a list.")
	public List<Customer> list() {
		return _customerRepository.findAll();
	}
	
	@GetMapping("/customers/{id}")
	@ApiOperation(value = "Returns a specific customer by given Id.")
	public Customer getById(@PathVariable(value="id") UUID id) {
		Customer customer = _customerRepository.findById(id).get(); 
		return customer;
	}
	
	@GetMapping("/customers/cpf/{cpf}")
	@ApiOperation(value = "Returns a specific customer by given CPF.")
	public Customer getByCpf(@PathVariable(value="cpf") String cpf) {
		Customer customer = _customerRepository.findByCpf(cpf).get();
		return customer;
	}
	
	@PostMapping("/customers")
	@ApiOperation(value = "Creates a new Customer.")
	public Customer createCustomer(@RequestBody Customer customer) {
		return _customerRepository.save(customer);
	}
	
	@DeleteMapping("/customers/{id}")
	@ApiOperation(value = "Deletes a customer by given Id.")
	public Customer deleteCustomer(@PathVariable(value="id") UUID id) {
		Customer c = _customerRepository.findById(id).get();	
		
		if (c == null) return null;
		_customerRepository.deleteById(id);
		
		return c;
	}
	
	@DeleteMapping("/customers/cpf/{cpf}")
	@ApiOperation(value = "Deletes a customer by given CPF.")
	public Customer deleteCustomerByCpf(@PathVariable(value="cpf") String cpf) {
		Customer c = _customerRepository.findByCpf(cpf).get();
		
		if (c == null) return null;		
		_customerRepository.deleteById(c.getId());
		
		return c;
	}
	
	@PutMapping("/customers/{id}")
	@ApiOperation(value = "Updates a customer by given Id.")
	public Customer updateCustomer(@RequestBody Customer customer, @PathVariable(value="id") UUID id) {
		Customer c = _customerRepository.findById(id).get();
		
		if (c == null) return null;
		
		return _customerRepository.save(customer);
	}
	
}
