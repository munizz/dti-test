package com.customers.api.models;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Entity
@Table(name="Customer")
public class Customer implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name="system-uuid", strategy="uuid2")
	@Type(type="pg-uuid")
	@Column(name="id")
	private UUID id;
	
	@Column(name="name", updatable = true, nullable = false)
	private String name;
	
	@Column(name="address", updatable = true, nullable = false)
	private String address;
	
	@Column(name="phone", updatable = true, nullable = false)
	private String phone;
	
	@Column(name="email", updatable = true, nullable = false)
	private String email;
	
	@Column(name="cpf", updatable = true, nullable = false)
	private String cpf;

	
	public Customer() {};
	
	public Customer(String name, String address, String phone, String email, String cpf) {
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.email = email;
		this.cpf = cpf;
	};
	
	public UUID getId() {
		return this.id;
	}
	
	public void setId(UUID uid) {
		this.id = uid;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAddress() {
		return this.address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getPhone() {
		return this.phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getCpf() {
		return this.cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
}

