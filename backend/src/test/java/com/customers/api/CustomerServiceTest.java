
package com.customers.api;

import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.customers.api.models.Customer;
import com.customers.api.repository.ICustomerRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class CustomerServiceTest {

	@Autowired
	private ICustomerRepository _customerRepository;
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void createCustomer() {
		Customer c = new Customer("John Malone", "Av. ABC, 123 - Small Town", "5531955774488", "jmalone@mail.com", "11122233344");
		this._customerRepository.save(c);
		
		Assertions.assertThat(c.getId()).isNotNull();
	}
	
	@Test
	public void updateCustomer() {
		Customer c = new Customer("John Malone", "Av. ABC, 123 - Small Town", "5531955774488", "jmalone@mail.com", "11122233344");
		this._customerRepository.save(c);
		
		c.setName("John Mclane");
		this._customerRepository.save(c);
		
		Assertions.assertThat(c.getName()).isNotEqualTo("John Malone");
	}
	
	@Test
	public void deleteCustomer() {
		Customer c = new Customer("John Malone", "Av. ABC, 123 - Small Town", "5531955774488", "jmalone@mail.com", "11122233344");
		this._customerRepository.save(c);
		this._customerRepository.delete(c);
		
		boolean exists = _customerRepository.findById(c.getId()).isPresent();
		Assertions.assertThat(!exists);
	}
}
